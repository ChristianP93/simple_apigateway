"use strict";

const ApiGateway = require("moleculer-web");
const DbService = require("moleculer-db");
const MongooseAdapter = require("moleculer-db-adapter-mongoose");

const errorhandler_log = require("../lib/errorhandler_log")
const performance_log = require("../lib/performance_log")

const isAuthenticated = require("./auth/auth");

module.exports = {
	name: "api",
	mixins: [ApiGateway],
	settings: {
		port: process.env.PORT || 3000,
		logRequestParams: "info",
		logResponseData: "debug",
		routes: [{
			path: "/api/v1",
			authorization: true,
			mappingPolicy: "restrict",

			onBeforeCall(ctx, route, req, res) {
				ctx.start = Date.now();
			},

			onAfterCall(ctx, route, req, res, data) {
				performance_log(req.url, req.headers.authorization, Date.now() - ctx.start)
				return data
			},

			onError(req, res, err) {
    		    res.setHeader("Content-Type", "text/plain");
				res.writeHead(err.code || 500);
				res.end("Route error: " + err.message);
				return errorhandler_log(req.headers.authorization, req.url, err.message, 500)
			},
			aliases: {

				"POST login/user/token": ["login.getUserToken"],
				"POST login/user": [isAuthenticated, "login.saveNewUser"],

				"POST product": [isAuthenticated, "product.saveProduct"],
				"GET product/:id" : [isAuthenticated, "product.getOneProduct"],
				"GET product/find/:limit/:offset"	: [isAuthenticated, "product.allProducts"],
				"DELETE product/delete/:id": [isAuthenticated, "product.removeProduct"],
				"PUT product/:id" : [isAuthenticated, "product.updateProduct"],
				"PUT product/blacklist/:id" : [isAuthenticated, "product.blacklistOneProduct"],
				"PUT product/noblacklist/:id" : [isAuthenticated, "product.noBlacklistOneProduct"],

				"POST prices/new/": [isAuthenticated, "prices.savePrices"],
				"GET prices/find"	: [isAuthenticated, "prices.allPrices"],
				"PUT prices/edit" : [isAuthenticated, "prices.updatePrices"],

				"POST blacklist/save": [isAuthenticated, "blacklist.saveWord"],
				"GET blacklist/find/one/:id" : [isAuthenticated, "blacklist.getOneWord"],
				"GET blacklist/find"	: [isAuthenticated, "blacklist.allWords"],
				"DELETE blacklist/delete/:id": [isAuthenticated, "blacklist.removeWord"],
				"PUT blacklist/update/:id" : [isAuthenticated, "blacklist.updateWord"]
			}

		}],

		rateLimit: {
    	    window: 60 * 1000,
            limit: 15,
            headers: true,
            key: (req) => {
                return req.headers["x-forwarded-for"] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
            },
        },

		onError(req, res, err) {
		  res.setHeader("Content-Type", "text/plain");
		  res.writeHead(500);
		  res.end("Global error: " + err.message);
			return errorhandler_log(req.headers.authorization, req.url, err.message, 500)
		},

		assets: {
			folder: "client_build"
		}
	}
};
